package com.test.codethatkills.notinum.main;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.test.codethatkills.notinum.R;
import com.test.codethatkills.notinum.adapter.CountryItemRecyclerViewAdapter;
import com.test.codethatkills.notinum.util.CountryItem;
import com.test.codethatkills.notinum.util.DataBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainFragment extends Fragment {

    private DataBase mDataBase;
    private List<CountryItem> mItems;
    private CountryItemRecyclerViewAdapter mAdapter;
    private Context mContext;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        mContext = getActivity();
        mDataBase = DataBase.getDBInstance(mContext);
        mDataBase.open();
        mItems = new ArrayList<>();
        mAdapter = new CountryItemRecyclerViewAdapter(mItems, mContext);
        readFromDataBase();

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(mAdapter);
        }
        return view;
    }

    private void readFromDataBase() {
        ArrayList<CountryItem> items = mDataBase.getCountryItems();
        if (items.size() == 0) {
            addCountriesToDataBase(parseResData());
            items = mDataBase.getCountryItems();
        }
        mItems.addAll(items);
        mAdapter.notifyDataSetChanged();
    }

    private List<CountryItem> parseResData() {
        ArrayList<CountryItem> countryItems = new ArrayList<>();
        ArrayList<String> stringsList = new ArrayList<>();
        stringsList.addAll(Arrays.asList(getResources().getStringArray(R.array.countries)));
        for (String item : stringsList) {
            countryItems.add(new CountryItem(item));
        }
        return countryItems;
    }

    private void addCountriesToDataBase(List<CountryItem> items) {
        int recAddError = 0;
        for (CountryItem item : items) {
            if (mDataBase.addCountryRec(item) == 0) recAddError++;
        }
        int recAdded = items.size() - recAddError;
        Toast.makeText(mContext, recAdded + " " + getString(R.string.data_added), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDataBase.close();
    }
}
