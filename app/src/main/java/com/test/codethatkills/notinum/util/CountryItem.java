package com.test.codethatkills.notinum.util;

public class CountryItem {

    public CountryItem() {
    }

    public CountryItem(String item) {
        String[] tokens = item.split(":");
        if (tokens.length > 1) {
            name = tokens[0];
            code = tokens[1];
        }
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private String name;
    private String code;
}
