package com.test.codethatkills.notinum.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DataBase {

    private static final String DB_NAME = "countriesDataBase.db";
    private static final String COUNTRIES_TABLE = "countries_table";

    private static final String COLUMN_ID = "_id";
    private static final String NAME = "name";
    private static final String CODE = "code";

    private static final int DB_VERSION = 1;

    private static final String COUNTRIES_DB_CREATE =
            "create table " + COUNTRIES_TABLE + "(" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    NAME + " text, " +
                    CODE + " text" +
                    ");";

    private final Context mCtx;
    private static DataBase db;
    private DBHelper mDBHelper;
    private SQLiteDatabase mDB;

    private DataBase(Context ctx) {
        mCtx = ctx;
    }

    public void open() {
        mDBHelper = new DBHelper(mCtx, DB_NAME, null, DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    public static DataBase getDBInstance(Context context) {
        return db == null ? db = new DataBase(context) : db;
    }

    public void close() {
        if (mDBHelper != null) mDBHelper.close();
    }

    public long addCountryRec(CountryItem item) {
        if (item == null) return 0;
        ContentValues cv = new ContentValues();
        cv.put(NAME, item.getName());
        cv.put(CODE, item.getCode());
        return mDB.insert(COUNTRIES_TABLE, null, cv);
    }

    public ArrayList<CountryItem> getCountryItems() {
        String countriesQuery = "SELECT  * FROM " + COUNTRIES_TABLE;
        Cursor c = mDB.rawQuery(countriesQuery, null);
        ArrayList<CountryItem> result = new ArrayList<>();
        if (c.moveToFirst()) {
            int nameColIndex = c.getColumnIndex(NAME);
            int codeColIndex = c.getColumnIndex(CODE);
            do {
                CountryItem item = new CountryItem();
                item.setName(c.getString(nameColIndex));
                item.setCode(c.getString(codeColIndex));
                result.add(item);
            } while (c.moveToNext());
        }
        return result;
    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                        int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(COUNTRIES_DB_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
