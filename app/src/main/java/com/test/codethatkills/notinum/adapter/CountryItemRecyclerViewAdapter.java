package com.test.codethatkills.notinum.adapter;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.test.codethatkills.notinum.R;
import com.test.codethatkills.notinum.util.CountryItem;

import java.util.List;


public class CountryItemRecyclerViewAdapter extends RecyclerView.Adapter<CountryItemRecyclerViewAdapter.ViewHolder> {

    private final List<CountryItem> mValues;
    private final Context mContext;

    public CountryItemRecyclerViewAdapter(List<CountryItem> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.country_item_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CountryItem currentItem = mValues.get(position);
        holder.mItem = currentItem;
        holder.mCountryName.setText(currentItem.getName());
        holder.mCountryCode.setText(currentItem.getCode());
        holder.mToastImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, currentItem.getName(), Toast.LENGTH_SHORT).show();
            }
        });
        holder.mNotificationImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resources res = mContext.getResources();
                NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
                builder.setSmallIcon(R.drawable.ic_launcher)
                        .setWhen(System.currentTimeMillis())
                        .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_launcher))
                        .setContentTitle(currentItem.getName())
                        .setContentText(currentItem.getName() + " " +
                                mContext.getString(R.string.code_is) + " " + currentItem.getCode());
                int notificationId = 3;
                NotificationManager manager = (NotificationManager)
                        mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                Notification notification = builder.build();
                manager.notify(notificationId, notification);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mCountryName;
        public final TextView mCountryCode;
        public final ImageButton mToastImageButton;
        public final ImageButton mNotificationImageButton;
        public CountryItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mCountryName = (TextView) view.findViewById(R.id.countryNameTextVIew);
            mCountryCode = (TextView) view.findViewById(R.id.countryCodeTextView);
            mToastImageButton = (ImageButton) view.findViewById(R.id.toastImageButton);
            mNotificationImageButton = (ImageButton) view.findViewById(R.id.notificationImageButton);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mCountryCode.getText() + "'";
        }
    }
}
